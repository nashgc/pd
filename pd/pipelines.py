# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DontCloseSpider
import psycopg2
import redis
import datetime
import mmh3
from urllib3.contrib.socks import SOCKSProxyManager


class PdPipeline(object):
    def open_spider(self, spider):
        hostname = 'localhost'
        username = 'postgres'
        password = 'P2ssw0rd'  # your password
        database = 'pd'

        # Postgres connection
        try:
            self.connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
            self.cur = self.connection.cursor()
        except Exception as e:
            spider.logger.critical('Postgres unable to connect - {}'.format(e))

        # Redis connection
        try:
            self.r_con = redis.Redis(host='localhost', port=6379, db=0)
        except Exception as e:
            spider.logger.critical('Redis unable to connect - {}'.format(e))

    def close_spider(self, spider):
        self.cur.close()
        self.connection.close()

    def process_item(self, item, spider):
        # RootSpider pipeline
        if item['info']['spider'] == 'RootSpider':
            url = item['onion']
            # clean up url, strip and then check for http:// and .onion
            url = url.strip('-')
            if url[:7] != 'http://':
                url = 'http://{}'.format(url)
            if url.endswith('.onion') == False:
                url = '{}.onion'.format(url)
            #check if url already exist
            try:
                self.cur.execute("select * from rootlinks where url= %s", (url,))
            except Exception as e:
                spider.logger.error('RootSpider can\'t select url - {}'.format(e))
            if self.cur.fetchone() is not None:
                pass
            else:
                proxy = SOCKSProxyManager('socks4a://127.0.0.1:9050')
                try:
                    f = open("not200.txt", "a+")
                    r = proxy.request('GET', url, timeout=20.0, retries=1)
                    if r.status == 200:
                        try:
                            self.cur.execute(
                                """insert into rootlinks(url, status, priority) values (%s, %s, %s);""",
                                (url, 'new', 0,))
                            self.connection.commit()
                        except Exception as e:
                            spider.logger.error('RootSpider pipeline error - {}'.format(e))
                            self.connection.rollback()
                    else:
                        f.write('{}\n'.format(url))
                        f.close()
                except Exception as e:
                    f.write('{} - {}\n'.format(url, e))
                    f.close()

        # ExplorerSpider pipeline
        elif item['info']['spider'] == 'ExplorerSpider':
            spider.logger.info('ExplorerSpider pipeline works')
            body = item['body']
            title = item['title']
            url = item['url']
            next_parse = datetime.date.today() + datetime.timedelta(days=1)
            last_parse = datetime.date.today()
            body_hash = mmh3.hash(body)
            try:
                self.cur.execute(
                    "insert into sites(body, title, url, next_parse, last_parse, hash)"
                    " values (%s,%s,%s,%s,%s,%s)", (body, title, url, next_parse, last_parse, body_hash))
                self.connection.commit()
            except Exception as e:
                spider.logger.error('ExplorerSpider postgres pipeline error - {}'.format(e))
                self.connection.rollback()
                raise DontCloseSpider('DONT CLOSEEEE')
            try:
                self.r_con.append(url, '')
            except Exception as e:
                spider.logger.error('ExplorerSpider redis pipeline error - {}'.format(e))
                raise DontCloseSpider('DONT CLOSEEEE')

        # UpdaterSpider pipeline
        elif item['info']['spider'] == 'UpdaterSpider':
            spider.logger.error('UpdaterSpider pipeline works')
            body = item['body']
            title = item['title']
            url = item['url']
            body_hash = mmh3.hash(body)
            self.cur.execute("select next_parse, last_parse, hash from sites where url= %s", (url,))
            url_data = self.cur.fetchall()
            data_hash = url_data[0][2]
            data_next_parse = url_data[0][0]
            data_last_parse = url_data[0][1]
            if int(data_hash) == int(body_hash):
                if data_next_parse - data_last_parse >= datetime.timedelta(days=60):
                    next_parse = datetime.date.today() + datetime.timedelta(days=60)
                    try:
                        self.cur.execute("update sites set next_parse= %s, last_parse= %s, status='free' where url= %s",
                                         (next_parse, datetime.date.today(), url))
                        self.connection.commit()
                    except Exception as e:
                        spider.logger.error('UpdaterSpider postgres pipeline error - {}'.format(e))
                        self.connection.rollback()
                        raise DontCloseSpider('DONT CLOSEEEE')
                else:
                    add_days = data_next_parse - data_last_parse + datetime.timedelta(days=7)
                    next_parse = datetime.date.today() + add_days
                    try:
                        self.cur.execute("update sites set next_parse= %s, last_parse= %s, status='free' where url= %s",
                                         (next_parse, datetime.date.today(), url))
                        self.connection.commit()
                    except Exception as e:
                        spider.logger.error('UpdaterSpider postgres pipeline error - {}'.format(e))
                        self.connection.rollback()
                        raise DontCloseSpider('DONT CLOSEEEE')
            else:
                next_parse = datetime.date.today() + datetime.timedelta(days=2)
                try:
                    self.cur.execute("update sites set next_parse= %s, last_parse= %s, status='free' where url= %s",
                                     (next_parse, datetime.date.today(), url))
                    self.connection.commit()
                except Exception as e:
                    spider.logger.error('ExplorerSpider postgres pipeline error - {}'.format(e))
                    self.connection.rollback()
                    raise DontCloseSpider('DONT CLOSEEEE')
                
                # next_parse = datetime.date.today() + datetime.timedelta(days=1)
                # last_parse = datetime.date.today()

