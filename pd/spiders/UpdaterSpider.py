from scrapy.spiders import CrawlSpider
import re
    
class UpdaterSpiderSpider(CrawlSpider):
    name = 'UpdaterSpider'
    allowed_domains = []
    start_urls = []
    login_page = ''
    
    def __init__(self, *args, **kwargs):
        """
        Init spider with params
        :param args: 
        :param kwargs: ad(allowed_domains), domains, row_id
        """
        # Add multiple allowed_domains without http://
        ad_list = kwargs.get('ad').split(',')
        for ad in ad_list:
            self.allowed_domains.append(ad)

        # Add multiple domain with http://
        domains = kwargs.get('domains').split(',')
        for domain in domains:
            self.start_urls.append(domain)
            
        self.login_page = 'http://{}'.format(kwargs.get('login_page'))
        self.row_id = kwargs.get('row_id')
        super(UpdaterSpiderSpider, self).__init__(*args, **kwargs)
    
    def parse(self, response):
        item = {}
        body_raw = u''.join(response.xpath('//body/descendant-or-self::*[not(self::script)]/text()').extract()).strip()
        body_raw = body_raw.replace('\n', '').replace('\r', '').replace('\t', '').strip()
        item['body'] = re.sub(r'\s(2,)', '\n', body_raw)
        title_raw = response.xpath('//head/title/text()').extract_first()
        item['title'] = title_raw.replace('\n', '').replace('\r', '').replace('\t', '').strip()
        item['url'] = response.url
        item['info'] = {'spider': 'UpdaterSpider', 'row_id': self.row_id}
        yield item
