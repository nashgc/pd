import logging
import psycopg2
import redis
import sys


def close_spider(reason, row_id, start_url):

    # Postgres connection
    hostname = 'localhost'
    username = 'postgres'
    password = 'P2ssw0rd'  # your password
    database = 'pd'

    try:
        connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
        pg_cur = connection.cursor()
    except Exception as e:
        logging.critical('Postgres unable to connect - {}'.format(e))
        sys.exit('Postgres unable to connect - {}'.format(e))

    # Redis connection
    try:
        r_con = redis.Redis(host='localhost', port=6379, db=0)
    except Exception as e:
        logging.critical('Redis unable to connect - {}'.format(e))

    if reason == 'finished':
        try:
            pg_cur.execute("""UPDATE rootlinks SET status='explored' WHERE url='{}'""".format(start_url))
            connection.commit()
        except Exception as e:
            logging.critical('Postgres can\'t update finish - {}'.format(e))
            sys.exit('Postgres can\'t update finish - {}'.format(e))
    else:
        # Postgres
        try:
            pg_cur.execute("""UPDATE rootlinks SET status='error' WHERE url='{}'""".format(start_url))
            connection.commit()
        except Exception as e:
            logging.critical('Postgres can\'t update error - {}'.format(e))
            sys.exit('Postgres can\'t update error - {}'.format(e))
        # Redis
        try:
            r_con.delete(start_url)
            logging.critical('Redis DELETE {}'.format(start_url))
        except Exception as e:
            logging.critical('Redis can\'t delete key error - {}'.format(e))
            sys.exit('Redis can\'t delete key error - {}'.format(e))
