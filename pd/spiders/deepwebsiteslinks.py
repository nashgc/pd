# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule


class DeepwebsiteslinksSpider(CrawlSpider):
    name = 'deepwebsiteslinks'
    allowed_domains = ['deepwebsiteslinks.com']
    start_urls = ['http://deepwebsiteslinks.com/']

    rules = (
        Rule(LinkExtractor(allow=''), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
        item = {}
        item['info'] = {'spider': 'RootSpider'}
        x = response.xpath('//p/span[@style="color: #ff0000;"]')
        for xx in x:
            item['onion'] = xx.xpath('.//text()').extract_first()
            yield item


