import logging, sys, datetime
import psycopg2
import redis

# logging pd_starter
logging.basicConfig(handlers=[logging.FileHandler('logs/pd_starter.txt', 'a', 'utf-8')], level=logging.INFO)
logging.info("/-------------------------- PD_STARTER START {} --------------------------/".format(datetime.datetime.now()))


# Redis connection
try:
    r_con = redis.Redis(host='localhost', port=6379, db=0)
except Exception as e:
    logging.critical('Redis unable to connect - {}'.format(e))
    sys.exit('Redis unable to connect - {}'.format(e))


# Postgres connection
hostname = 'localhost'
username = 'postgres'
password = 'P2ssw0rd'  # your password
database = 'pd'

try:
    connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    pg_cur = connection.cursor()
except Exception as e:
    logging.critical('Postgres unable to connect - {}'.format(e))
    sys.exit('Postgres unable to connect - {}'.format(e))



try:
    pg_cur.execute("""SELECT url from sites"")
    rows = pg_cur.fetchall()
except Exception as e:
    logging.critical('Postgres error - {}'.format(e))
    sys.exit('Postgres error - {}'.format(e))


# Now fill Redis with data
for row in list(rows):
    if row[0] == None:
        r_con.set('None', '')
    else:
        r_con.set(row[0], '')


