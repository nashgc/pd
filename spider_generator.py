import os


def spider_generator(type, login=False):

    spider_name = f"""{'ExplorerSpider' if type == 'explorer' else 'UpdaterSpider'}{'Login' if login == True else ''}"""
    parse_func_name = f"""{"parse_item" if type == 'explorer' or type != 'explorer' and login == True else "parse"}"""
    spiders_path = os.path.join('pd', 'spiders')
    spider_full_path = os.path.join(spiders_path, spider_name)


    import_statement = """import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request, FormRequest
from .close_func import close_spider
import re
    """

    class_defenition = f"""
class {spider_name}Spider(CrawlSpider):
    name = '{spider_name}'
    allowed_domains = []
    start_urls = []
    login_page = ''
    """

    init_defenition = f"""
    def __init__(self, *args, **kwargs):
        \"""
        Init spider with params
        :param args: 
        :param kwargs: ad(allowed_domains), domains, row_id
        \"""
        # Add multiple allowed_domains without http://
        ad_list = kwargs.get('ad').split(',')
        for ad in ad_list:
            self.allowed_domains.append(ad)

        # Add multiple domain with http://
        domains = kwargs.get('domains').split(',')
        for domain in domains:
            self.start_urls.append(domain)
            
        self.login_page = 'http://{{}}'.format(kwargs.get('login_page'))
        self.row_id = kwargs.get('row_id')
        super({spider_name}Spider, self).__init__(*args, **kwargs)
    """


    rules_defenition = """
    rules = (
        Rule(LinkExtractor(), callback='parse_item', follow=True),
    )
    """

    login_defenition = f"""
    def login(self, response):
        token = response.xpath('//*[@name="csrf_token"]/@value').extract_first()
        return FormRequest.from_response(response,
                                         formdata={{'csrf_token': token,
                                            'username': 'vlanblog',
                                            'password': 'P2ssw0rd'}},
                                         {"callback=self.parse_item" if type != 'explorer' else ''}
                                         )"""

    start_request_defenition = """
    def start_requests(self):
        yield Request(url=self.login_page, callback=self.login, dont_filter=True)
    """


    parse_item_defenition = f"""
    def {parse_func_name}(self, response):
        item = {{}}
        body_raw = u''.join(response.xpath('//body/descendant-or-self::*[not(self::script)]/text()').extract()).strip()
        body_raw = body_raw.replace('\\n', '').replace('\\r', '').replace('\\t', '').strip()
        item['body'] = re.sub(r'\s{2,}', '\\n', body_raw)
        title_raw = response.xpath('//head/title/text()').extract_first()
        item['title'] = title_raw.replace('\\n', '').replace('\\r', '').replace('\\t', '').strip()
        item['url'] = response.url
        item['info'] = {{'spider': '{spider_name}', 'row_id': self.row_id}}
        yield item
        """

    close_func_defenition = """
    def closed(self, reason):
        close_spider(reason, row_id=self.row_id, start_url=self.start_urls[0])
    """




    f = open(f"{spider_full_path}.py", 'w')
    if type == 'explorer' and login == False:
        f.write(import_statement + class_defenition + init_defenition +
                rules_defenition + parse_item_defenition + close_func_defenition)
    elif type == 'explorer' and login == True:
        f.write(import_statement + class_defenition + init_defenition + rules_defenition
                + start_request_defenition + login_defenition + parse_item_defenition + close_func_defenition)
    elif type != 'explorer' and login == False:
        f.write(import_statement + class_defenition + init_defenition + parse_item_defenition)
    elif type != 'explorer' and login == True:
        f.write(import_statement + class_defenition + init_defenition
                + start_request_defenition + login_defenition + parse_item_defenition)
    f.close()


    # if type == 'explorer' and login == False:
    #     print(import_statement + class_defenition + init_defenition + rules_defenition + parse_item_defenition)
    # elif type == 'explorer' and login == True:
    #     print(import_statement + class_defenition + init_defenition + rules_defenition
    #             + start_request_defenition + login_defenition + parse_item_defenition)
    # elif type != 'explorer' and login == False:
    #     print(import_statement + class_defenition + init_defenition + parse_item_defenition)
    # elif type != 'explorer' and login == True:
    #     print(import_statement + class_defenition + init_defenition
    #             + start_request_defenition + login_defenition + parse_item_defenition)



spider_generator(type='explorer')