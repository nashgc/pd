import logging, sys, datetime, re, os
import psycopg2
from scrapyd_api import ScrapydAPI
from urllib.parse import urlparse


# logging pd_starter
current_dir_path = os.path.dirname(os.path.realpath(__file__))
logs_dir = os.path.join(current_dir_path, 'logs')
logging.basicConfig(handlers=[logging.FileHandler(os.path.join(logs_dir, 'pd_manager.txt'), 'a', 'utf-8')], level=logging.INFO)
logging.info("/-------------------------- MANAGER START {} --------------------------/".format(datetime.datetime.now()))


# Postgres connection
hostname = 'localhost'
username = 'postgres'
password = 'P2ssw0rd'  # your password
database = 'pd'

try:
    connection = psycopg2.connect(host=hostname, user=username, password=password, dbname=database)
    connection.autocommit = False
    pg_cur = connection.cursor()
except Exception as e:
    logging.critical('Postgres unable to connect - {}'.format(e))
    sys.exit('Postgres unable to connect - {}'.format(e))

# Scrapyd work with
scrapyd = ScrapydAPI('http://localhost:6800')


def GetSpidersOptions():
    '''
    Get spiders options from DB 'manager_conf' table
    :return: dict of options
    '''
    try:
        result = {}
        # pg_cur.execute("""SELECT option_value FROM manager_conf WHERE option_name='active_spiders' """)
        pg_cur.execute("""SELECT option_name, option_value FROM manager_conf WHERE option_name IN ('ExplorerSpider', 'UpdaterSpider')""")
        rows = pg_cur.fetchall()
        for key, value in rows:
            result[key] = value
        return result
    except Exception as e:
        logging.critical('Postgres error - {}'.format(e))
        sys.exit('Postgres error - {}'.format(e))


def CountRunningSpiders():
    ExplorerSpider = 0
    UpdaterSpider = 0

    try:
        scrapy_jobs = scrapyd.list_jobs('pd')
    except Exception as e:
        logging.critical('Scrapyd error - {}'.format(e))
        sys.exit('Scrapyd error - {}'.format(e))

    for job in scrapy_jobs['running']:
       if re.match(r'^ExplorerSpider', job['spider']):
           ExplorerSpider += 1
       if re.match(r'^UpdaterSpider', job['spider']):
           UpdaterSpider += 1
    return {'ExplorerSpider': ExplorerSpider, 'UpdaterSpider': UpdaterSpider}


def get_ad_and_domains_str(next_url):
    urls_list = []
    ad_list = set()
    for url in next_url:
        urls_list.append(url[1])
        ad_list.add(urlparse(url[1]).netloc)
    urls_string = ','.join(urls_list)
    ad_string = ','.join(ad_list)
    return ad_string, urls_string


def ScheduleSpider(flow, spider_name, next_url, login=False):
    try:
        ad_str, domains_str = get_ad_and_domains_str(next_url)
        if login == True:
            scrapyd.schedule(
                project='pd', spider=spider_name, ad=ad_str, domains=domains_str,
                row_id=next_url[0], login_page=next_url[4]
            )
        else:
            scrapyd.schedule(project='pd', spider=spider_name, ad=ad_str, domains=domains_str)
        message = 'Scrapyd flow: {} name: {} was scheduled'.format(flow, spider_name)
        logging.info(message)
    except Exception as e:
        message = 'Scrapyd flow: {} name: {} schedule error - {}'.format(flow, spider_name, e)
        logging.critical(message)
        sys.exit(message)


def RunSpider():
    '''
    Run Spider!!!
    :return:
    '''

    # Get information about spiders
    spiders_options = GetSpidersOptions()
    running_spiders_num = CountRunningSpiders()

    # Statistic
    running_spiders_info = {}
    running_spiders_info['OPTIONS'] = spiders_options
    running_spiders_info['RUNNING SPIDERS'] = running_spiders_num

    for spider in ['ExplorerSpider', 'UpdaterSpider']:
        # spiders activity calculation
        if int(running_spiders_num[spider]) < int(spiders_options[spider]):
            # How many spiders we have to run
            need_spiders_num = int(spiders_options[spider]) - int(running_spiders_num[spider])
            running_spiders_info[spider] = ' NEED TO RUN {}'.format(need_spiders_num)
            for count in range(need_spiders_num):
                # EXPLORER SPIDER RUN LOGIC
                if spider == 'ExplorerSpider':
                    try:
                        pg_cur.execute("""SELECT
 sq.*
FROM (
  SELECT r.*
  FROM rootlinks r
  WHERE r.status='new'
  ORDER BY r.priority DESC, r.id ASC
) sq
LIMIT 1""")
                        next_url = pg_cur.fetchall()
                        # TODO: ADD LOGIN REQ LOGIC HERE!
                        # if login_req true...
                        # if next_url[3] == True:
                        #     spider_name_login_req = GetSpiderNameByUrl(next_url[1])
                        #     ScheduleSpider(flow=spider, spider_name=spider_name_login_req, next_url=next_url, login=True)
                        #     pg_cur.execute("""UPDATE next_url SET status='parsing' WHERE id={}""".format(next_url[0]))
                        #     connection.commit()
                        # # if login_req false...
                        # else:
                        ScheduleSpider(flow=spider, spider_name=spider, next_url=next_url)
                        pg_cur.execute("UPDATE rootlinks SET status='parsing' WHERE url= %s", (next_url[0][1]))
                        connection.commit()
                    except Exception as e:
                        logging.critical('explorer_spider psycopg2 error (transaction wasn\'t commited ) - {}'.format(e))
                        sys.exit('explorer_spider psycopg2 error (transaction wasn\'t commited ) - {}'.format(e))
                        connection.rollback()
                # UPDATER SPIDER RUN LOGIC
                elif spider == 'UpdaterSpider':
                    try:
                        pg_cur.execute(
                            "select * from sites where next_parse<=NOW() and status='free' ORDER BY next_parse ASC limit 10")
                        next_url = pg_cur.fetchall()
                    except Exception as e:
                        logging.critical('updater_spider psycopg2 error (can\'t select ) - {}'.format(e))
                        sys.exit('updater_spider psycopg2 error (can\'t select ) - {}'.format(e))

                    try:
                        for url in next_url:
                            pg_cur.execute("update sites set status='parsing' where url= %s", (url[1],))
                            connection.commit()
                    except Exception as e:
                        logging.critical('updater_spider psycopg2 error (can\'t update ) - {}'.format(e))
                        sys.exit('updater_spider psycopg2 error (can\'t update ) - {}'.format(e))

                    ScheduleSpider(flow=spider, spider_name=spider, next_url=next_url)
        else:
            pass
    connection.close()
    for k, v in running_spiders_info.items():
        print('{}-{}'.format(k, v))
RunSpider()
